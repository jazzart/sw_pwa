var CACHE_NAME = "${HASH}";
var URLS_CACHE = ${PAGE_LIST};  // AEM will place array of paths into this binding

//----------------------------------------------------------------

var CACHE_NAME_DYNAMIC = CACHE_NAME + '_dynamic';
//var domain = 'https://www-foundationmedicineasia-com.opentemplate.dev.opengarden.rch.cm';
//var modifiedArray = [];
//CACHE_NAME.forEach(function(el) {
//  var path = el += ".html";
//  modifiedArray.push(path);
//});
//
//CACHE_NAME = modifiedArray;

URLS_CACHE.push("/");
console.log("URLS_CACHE: " + URLS_CACHE);

self.addEventListener('install', function (event) {
    console.log('Installing Service Worker ...', event);
    event.waitUntil(
        caches.open(CACHE_NAME)
            .then(function (cache) {
                //console.log('Precaching App Shell');
                //cache.addAll(URLS_CACHE);

                URLS_CACHE.forEach(function(el) {
                    if (el.length > 0) {
                        cache.add(el);
                    }
                });
            })
    )
});

self.addEventListener('activate', function (event) {
 console.log('Activating Service Worker ....', event);
 event.waitUntil(
   caches.keys()
     .then(function (keyList) {
       return Promise.all(keyList.map(function (key) {
         // kill old and deactivated sw-caches
         if (key !== CACHE_NAME && key !== CACHE_NAME_DYNAMIC) {
           console.log('Removing old cache.', key);
           return caches.delete(key);
         }
       }));
     })
 );
 return self.clients.claim();
});

function inUrlArray(string, array) {
    var cachePath;
    if (string.indexOf(self.origin) === 0) { // request targets domain where we serve the page from (i.e. NOT a CDN)
        //console.log('matched ', string);
        cachePath = string.substring(self.origin.length); // take the part of the URL AFTER the domain (e.g. after localhost:8080)
    } else {
        cachePath = string; // store the full request (for CDNs)
    }
    return array.indexOf(cachePath) > -1;
}

self.addEventListener('fetch', function (event) {

    var urlTest = inUrlArray(event.request.url, URLS_CACHE);
    if (urlTest) {
        event.respondWith(
            caches.open(CACHE_NAME)
                .then(function (cache) {
                    return fetch(event.request)
                        .then(function (res) {
                            cache.put(event.request.url, res.clone()).catch(function(err) {
                                console.log("error with PUT: " + err);
                            });
                            return res;
                        });
                })
        )
    } else {
        event.respondWith(
            caches.match(event.request)
                .then(function (response) {
                    if (response) {
                        console.log("response in cache");
                        return response;
                    } else {
                        return fetch(event.request)
                            .then(function (res) {
                                return caches.open(CACHE_NAME)
                                    .then(function (cache) {
                                        cache.put(event.request.url, res.clone());
                                        return res;
                                    })
                            })
                            .catch(function (err) {
                                return caches.open(CACHE_NAME)
                                    .then(function (cache) {
                                        if (event.request.headers.get('accept').includes('text/html')) {
                                            return cache.match('/offline.html');
                                        }
                                    });
                            });
                    }
                })
        );
    }
});